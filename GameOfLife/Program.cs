﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static int policzSasiadow(bool[,] plansza, int x, int y)
        {
            int arraySize = plansza.GetLength(1);
            int sasiedzi = 0;
            for (int i = x - 1; i <= x + 1; ++i)
            {
                for (int j = y - 1; j <= y + 1; ++j)
                {

                    if (i == x && j == y)
                    {
                        continue;
                    }

                    if ((i >= 0 && j >= 0 && i < arraySize && j < arraySize)
                        && plansza[i, j])
                    {
                        ++sasiedzi;
                    }
                }
            }
            return sasiedzi;
        }


        static bool[,] turaGry(bool[,] plansza)
        {
            bool[,] nowaPlansza = new bool[plansza.GetLength(0), plansza.GetLength(1)];
            Array.Copy(plansza, nowaPlansza, plansza.Length);
            for (int i = 0; i < plansza.GetLength(0); ++i)
            {
                for (int j = 0; j < plansza.GetLength(1); ++j)
                {
                    var sasiedzi = policzSasiadow(plansza, i, j);
                    if (!plansza[i, j] && sasiedzi == 3)
                    {
                        nowaPlansza[i, j] = true;
                    }
                    else if (plansza[i, j])
                    {
                        if (sasiedzi < 2 || sasiedzi > 3)
                        {
                            nowaPlansza[i, j] = false;
                        }
                    }

                }
            }
            return nowaPlansza;
        }

        static void wyswietlPlansze(bool[,] plansza)
        {
            for (int i = 0; i < plansza.GetLength(0); ++i)
            {
                for (int j = 0; j < plansza.GetLength(1); ++j)
                {
                    Console.Write(plansza[i, j] ? 1 : 0);
                    Console.Write(' ');
                }
                Console.Write('\n');
            }
        }

        static void Main(string[] args)
        {
            bool[,] plansza = { { true, true, true }, { true, true, true }, { false, false, true } };
            while (true)
            {
                wyswietlPlansze(plansza);
                plansza = turaGry(plansza);
                Console.WriteLine(' ');
                Console.ReadKey();
            }
        }
    }
}
